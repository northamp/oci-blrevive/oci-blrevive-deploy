variable "ocid_fingerprint" {
  description = "Fingerprint of oci api private key"
  type        = string
}

variable "ocid_private_key" {
  description = "Path to oci api private key used"
  type        = string
}

variable "ocid_region" {
  description = "The oci region where resources will be created"
  type        = string
}

variable "ocid_tenancy" {
  description = "Tenancy ocid where to create the sources"
  type        = string
}

variable "ocid_user" {
  description = "Ocid of user that terraform will use to create the resources"
  type        = string
}

variable "ocid_compartment" {
  description = "Compartment ocid where to create all resources"
  type        = string
}
