# OCI Blacklight: Revive Deployment pipeline project

This project's goal is to provide a deployment template for a Blacklight: Revive server that runs on an Oracle Cloud Infrastructure ARM machine **that fits in the free tier**.

It relies on a [Terraform module](https://gitlab.com/northamp/oci-blrevive/oci-blrevive-terraform) and an Ansible playbook (pending creation).

All it requires are:

* A copy of Blacklight: Retribution uploaded on [mega](https://mega.io/)
  * It can be yours, a friend's, Google's, doesn't matter
  * Instructions on how to prepare one can be found below (TODO)
* An [Oracle Cloud Free Tier](https://www.oracle.com/cloud/free/) account with a tenancy created
  * It *really* is free, but like every free things it comes with no guarantees, just like this project :)
  * You'll need to enter a credit card for verification purposes. I've never been charged personally, just make sure *not* to upgrade once your account is created
  * Your home region **cannot** be changed, so make sure to pick one and be prepared to stick to it (tenancy [deletion](https://www.reddit.com/r/oraclecloud/comments/rwwd78/free_tier_resources_in_different_region/) [is a](https://www.reddit.com/r/VPS/comments/zwbq6j/change_oracle_free_tier_region/) [myth](https://community.oracle.com/customerconnect/discussion/621022/how-to-delete-tenancy-and-change-home-region))
* A Gitlab account to run pipelines on
  * New accounts should be able to use shared runners just fine

## Preparation

### Fork

Begin by forking this project. Do so by clicking on the appropriately named `Fork` button on this very page.

Pick the namespace that's the most appropriate to you, usually the one that matches your name, and fork away.

### Blacklight: Retribution on a file sharing service

TODO

### Oracle Cloud

This step assumes you've already subscribed to Oracle Cloud Free Tier and have gained access to the Oracle console by logging into your tenant (i.e. following this [link](https://cloud.oracle.com/identity/domains/my-profile)).

You will now have to prepare your account to be able to run the server deployment job by:

* Generating API keys
* Creating a compartment

### API Keys

Browse to the Oracle Cloud Infrastructure dashboard, head over to [your profile/API keys](https://cloud.oracle.com/identity/domains/my-profile/api-keys), and click `Add API key`.

Keep the selection on `Generate`, download both the public and private key files (you'll especially need the private key, so **note the file name's down**) and click on `Add`.

![API Key generation](docs/_images/oci-apikey_gen.png)

The parameters in the window that follows are very important, make sure to note them down:

![Configuration file preview](docs/_images/oci-apikey_preview.png)

Each parameters correspond to the following environment variable:

| `Configuration file` name | Key                       | Flags                 |
| ------------------------- | ------------------------- | --------------------- |
| `user`                    | `TF_VAR_ocid_user`        | `Protected`, `Masked` |
| `fingerprint`             | `TF_VAR_ocid_fingerprint` | `Protected`, `Masked` |
| `tenancy`                 | `TF_VAR_ocid_tenancy`     | `Protected`, `Masked` |
| `region`                  | `TF_VAR_ocid_region`      |                       |

Head over to your project's CI/CD variables (`Settings`->`CI/CD`->Expand `Variables`) and create them, resulting in something like this:

![Terraform CI/CD Variables](docs/_images/oci-apikey_cicd_vars.png)

You'll also need to create a var named `TF_VAR_ocid_private_key` of the type `File` with the flag `Protected`, which has the content of the private key file you previously downloaded.

![API Key var](docs/_images/oci-apikey_file_var.png)

Once that is sorted out, only the compartment remains before your tenancy is ready to run Blacklight.

### Compartment

Head over to the [compartment screen](https://cloud.oracle.com/identity/compartments), create one named however you want, and refresh the page to make it show up.

![Compartment creation](docs/_images/oci-compartment_create.png)

Once done, copy the compartment ID by hovering over it...

![Compartment ID copy](docs/_images/oci-compartment_copy.png)

... And just like the API key before, you'll need to create an environment variable in your project: this time, name it `TF_VAR_ocid_compartment`, with the flags `Protected` and `Masked`, and the content being the value you've just copied.

You should end up with variables looking like this:

![Compartment ID](docs/_images/oci-cicd_final.png)

If that's the case, chances are you're all set up to deploy new virtual machines on your Oracle account with Terraform using Gitlab CI/CD, congrats!
