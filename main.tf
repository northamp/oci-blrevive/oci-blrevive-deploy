terraform {
  backend "http" {
  }
}

module "blrevive-oci" {
  source  = "gitlab.com/northamp/blrevive/oci"
  version = "1.0.1"

  ocid_region      = var.ocid_region
  ocid_tenancy     = var.ocid_tenancy
  ocid_user        = var.ocid_user
  ocid_compartment = var.ocid_compartment
  ocid_fingerprint = var.ocid_fingerprint
  ocid_private_key = var.ocid_private_key
}