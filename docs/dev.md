<!-- markdownlint-disable MD024 -->
# Development notes

## Running BLRE on ARM64

### Using Box86

[Link](https://github.com/ptitSeb/box86)

#### Script

```bash
sudo apt install -y git cmake gcc-arm-linux-gnueabihf

# https://ptitseb.github.io/box86/COMPILE.html
git clone https://github.com/ptitSeb/box86
cd box86
mkdir build
cd build
cmake .. -DARM64=1 -DCMAKE_BUILD_TYPE=RelWithDebInfo
make -j3
```

#### Result

Breaks, binary cannot be used. Gotta use a armhf chroot apparently.

### Using an x86 chroot (QEMU)

[Link](https://danct12.github.io/x86-chroot-on-ARM/)

#### Script

```bash
sudo apt install qemu-user-static schroot debootstrap

sudo debootstrap --arch i386 --foreign bookworm /srv/chroot/debian-x86 http://debian.xtdv.net/debian

sudo cp /usr/bin/qemu-i386-static /srv/chroot/debian-x86/usr/bin

sudo bash -c 'cat > /etc/schroot/chroot.d/debianx86.conf << EOF
[debian-x86]
description=Debian Bookworm x86 chroot
aliases=debian-x86
type=directory
directory=/srv/chroot/debian-x86
profile=desktop
personality=linux
preserve-environment=true
EOF'
```

#### Result

Works okay, can chroot successfully and install x86 packages, but it seems *slow*.

Can be used as last resort I suppose.

### Using an ARMHF chroot (no QEMU)

[Link](https://forum.armbian.com/topic/16584-install-box86-on-arm64/)
[Link2](https://github.com/ptitSeb/box86/blob/master/docs/X86WINE.md)

#### Script

```bash
# firewall tweaks
sudo iptables -I INPUT 5 -p tcp --dport 7778 -j ACCEPT
sudo iptables -I INPUT 5 -p udp --dport 7777 -j ACCEPT

sudo apt install schroot debootstrap unzip

sudo mkdir -p /srv/chroot/debian-armhf
sudo debootstrap --arch armhf --foreign bookworm /srv/chroot/debian-armhf http://debian.xtdv.net/debian
sudo chroot "/srv/chroot/debian-armhf" /debootstrap/debootstrap --second-stage

sudo bash -c 'cat > /etc/schroot/chroot.d/debian-armhf.conf << EOF
[debian-armhf]
description=Debian Armhf chroot
aliases=debian-armhf
type=directory
directory=/srv/chroot/debian-armhf
profile=desktop
personality=linux
preserve-environment=true
EOF'

sudo bash -c 'cat > /etc/schroot/desktop/nssdatabases << EOF
shadow
gshadow
services
protocols
EOF'

sudo bash -c 'cat > /srv/chroot/debian-armhf/var/lib/dpkg/statoverride << EOF
root root 2755 /usr/bin/crontab
EOF'

sudo schroot -c debian-armhf

# Inside the chroot, as root

apt install -y git wget cmake build-essential python3 gcc-arm-linux-gnueabihf xvfb cabextract
git clone https://github.com/ptitSeb/box86
cd box86
mkdir build
cd build
cmake .. -DARM_DYNAREC=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo
make -j4
cp box86 /usr/local/bin/box86

# All done, now onto Wine
wbranch="stable"
wversion="7.0.2"
wid="debian"
wdist="bookworm"
wtag="-1"

wget https://dl.winehq.org/wine-builds/${wid}/dists/${wdist}/main/binary-i386/wine-${wbranch}-i386_${wversion}~${wdist}${wtag}_i386.deb
wget https://dl.winehq.org/wine-builds/${wid}/dists/${wdist}/main/binary-i386/wine-${wbranch}_${wversion}~${wdist}${wtag}_i386.deb
dpkg-deb -x wine-${wbranch}-i386_${wversion}~${wdist}${wtag}_i386.deb wine-installer
dpkg-deb -x wine-${wbranch}_${wversion}~${wdist}${wtag}_i386.deb wine-installer
mv wine-installer/opt/wine* ~/wine
rm wine*.deb
rm -rf wine-installer

ln -s ~/wine/bin/wine /usr/local/bin/wine
ln -s ~/wine/bin/wineboot /usr/local/bin/wineboot
ln -s ~/wine/bin/winecfg /usr/local/bin/winecfg
ln -s ~/wine/bin/wineserver /usr/local/bin/wineserver
chmod +x /usr/local/bin/wine /usr/local/bin/wineboot /usr/local/bin/winecfg /usr/local/bin/wineserver

dpkg --add-architecture armhf && sudo apt update
apt install -y libasound2:armhf libc6:armhf libglib2.0-0:armhf libgphoto2-6:armhf libgphoto2-port12:armhf \
    libgstreamer-plugins-base1.0-0:armhf libgstreamer1.0-0:armhf libopenal1:armhf libpcap0.8:armhf \
    libpulse0:armhf libsane1:armhf libudev1:armhf libusb-1.0-0:armhf libvkd3d1:armhf libx11-6:armhf libxext6:armhf \
    libasound2-plugins:armhf ocl-icd-libopencl1:armhf libncurses6:armhf libncurses5:armhf libcap2-bin:armhf libcups2:armhf \
    libdbus-1-3:armhf libfontconfig1:armhf libfreetype6:armhf libglu1-mesa:armhf libglu1:armhf libgnutls30:armhf \
    libgssapi-krb5-2:armhf libkrb5-3:armhf libodbc1:armhf libosmesa6:armhf libsdl2-2.0-0:armhf libv4l-0:armhf \
    libxcomposite1:armhf libxcursor1:armhf libxfixes3:armhf libxi6:armhf libxinerama1:armhf libxrandr2:armhf \
    libxrender1:armhf libxxf86vm1 libc6:armhf libcap2-bin:armhf

# If you keep the WINEDEBUG var, you better get ready for a colossal amount of Wine puke - but at least, long as it does, it means it's running!
WINEDEBUG=+all box86 wine wineboot

# winetricks
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod +x winetricks
# now onto weird stuff
cd /home/ubuntu
box86 box86/tests/bash
./winetricks d3dx9 xact
exit

# Exit chroot and run stuff from the outside
exit

# Now entering the fun part - BLRE
curl -LO https://github.com/SteamRE/DepotDownloader/releases/download/DepotDownloader_2.5.0/DepotDownloader-linux-arm64.zip
unzip DepotDownloader-linux-arm64.zip
rm DepotDownloader-linux-arm64.zip
chmod +x DepotDownloader

STEAM_USERNAME=enter me
curl -LO https://gitlab.com/-/snippets/2529720/raw/main/filelist.txt
./DepotDownloader -app 209870 -depot 209871 -manifest 8740515310020658800 -username $STEAM_USERNAME -filelist filelist.txt

ln -s depots/209871/2520205/ blacklightre

# Acquire patched blacklight binaries here. Good luck

# Back inside chroot to start the game
sudo schroot -c debian-armhf
box86 box86/tests/bash

export DISPLAY=:9874
Xvfb :9874 -screen 0 1024x768x16 &

mkdir -p blacklightre/FoxGame/Config/BLRevive/server_utils/
cat > blacklightre/FoxGame/Config/BLRevive/default.json << EOF
{
  "Proxy": {
    "Server": {
      "Host": "0.0.0.0",
      "Port": "+1"
    },
    "Modules": {
      "Server": [
        "server-utils",
        "stats-uploader"
      ],
      "Client": [
    ]
    },
    "EnableConsole": false
  }
}
EOF
cat > blacklightre/FoxGame/Config/BLRevive/server_utils/server_config.json << EOF
{
    "hacks": {
        "disableOnMatchIdle": 1
    },
    "mutators": {
        "DisableDepots": 0,
        "DisableElementalAmmo": 0,
        "DisableGear": 0,
        "DisableHRV": 0,
        "DisableHeadShots": 0,
        "DisableHealthRegen": 0,
        "DisablePrimaries": 0,
        "DisableSecondaries": 0,
        "DisableTacticalGear": 0,
        "HeadshotsOnly": 0,
        "StockLoadout": 0
    },
    "properties": {
        "GameForceRespawnTime": 30.0,
        "GameRespawnTime": 3.0,
        "GameSpectatorSwitchDelayTime": 120.0,
        "GoalScore": 4500,
        "MaxIdleTime": 180.0,
        "MinRequiredPlayersToStart": 1,
        "NumEnemyVotesRequiredForKick": 4,
        "NumFriendlyVotesRequiredForKick": 2,
        "PlayerSearchTime": 40.0,
        "RandomBotNames": [
            "Frag Magnet",
            "Spook",
            "OOMKilled",
            "Rakbhu",
            "Server Fault",
            "kernel panic",
            "WINE_CXX_EXCEPTION",
            "ACCESS_VIOLATION",
            "CODE c0000005",
            "Firestarter",
            "Stainless Kill",
            "Bomberman",
            "Wireshark",
            "Sugarfree bot",
            "A real player",
            "James Bot",
            "Quilkin",
            "Clapbot",
            "Prometheus"
        ],
        "TimeLimit": 10,
        "VoteKickBanSeconds": 1200
    },
    "webserver": {
        "Enabled": true,
        "Host": "0.0.0.0",
        "Port": 7778
    }
}
EOF

wine blacklightre/Binaries/Win32/BLRevive.exe server HeloDeck
```

Holy shit it ~~works~~ *starts*

Need to test performance, ~~but it looks great so far.~~ see [Discord](https://discord.com/channels/794628005342216203/794630822627115028/1182727905105674330)

```bash
ubuntu@blre0:~$ curl -s 127.0.0.1:7778/server_info | jq
{
  "BotCount": 0,
  "GameMode": "TDM",
  "GameModeFullName": "Team Deathmatch",
  "GoalScore": 4500,
  "Map": "helodeck",
  "MaxPlayers": 16,
  "Mutators": {
    "DisableDepots": false,
    "DisableElementalAmmo": false,
    "DisableGear": false,
    "DisableHRV": false,
    "DisableHeadShots": false,
    "DisableHealthRegen": false,
    "DisablePrimaries": false,
    "DisableSecondaries": false,
    "DisableTacticalGear": false,
    "HeadshotsOnly": false,
    "HealthModifier": 1,
    "StaminaModifier": 1,
    "StockLoadout": false
  },
  "PlayerCount": 0,
  "Playlist": "None",
  "RemainingTime": 600,
  "ServerName": "",
  "TeamList": [],
  "TimeLimit": 600
}
ubuntu@blre0:~$ sudo netstat -antp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:7778            0.0.0.0:*               LISTEN      8029/wineserver
[...]
ubuntu@blre0:~$ sudo netstat -anup
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
udp        0      0 0.0.0.0:7777            0.0.0.0:*                           8015/wine
[...]
ubuntu@blre0:~$ uname -m
aarch64
```

### Using FEX

[Link](https://github.com/FEX-Emu/FEX)

#### Script

```bash
# firewall tweaks
sudo iptables -I INPUT 5 -p tcp --dport 7777 -j ACCEPT
sudo iptables -I INPUT 5 -p udp --dport 7777 -j ACCEPT

sudo apt update && sudo apt install -y cabextract unzip xvfb

curl --silent https://raw.githubusercontent.com/FEX-Emu/FEX/main/Scripts/InstallFEX.py --output /tmp/InstallFEX.py && python3 /tmp/InstallFEX.py && rm /tmp/InstallFEX.py

FEXBash

# All done, now onto Wine
wbranch="stable"
wversion="7.0.2"
wid="debian"
wdist="bookworm"
wtag="-1"

sudo -E wget https://dl.winehq.org/wine-builds/${wid}/dists/${wdist}/main/binary-i386/wine-${wbranch}-i386_${wversion}~${wdist}${wtag}_i386.deb
sudo -E wget https://dl.winehq.org/wine-builds/${wid}/dists/${wdist}/main/binary-i386/wine-${wbranch}_${wversion}~${wdist}${wtag}_i386.deb
sudo -E dpkg-deb -x wine-${wbranch}-i386_${wversion}~${wdist}${wtag}_i386.deb wine-installer
sudo -E dpkg-deb -x wine-${wbranch}_${wversion}~${wdist}${wtag}_i386.deb wine-installer
sudo -E mv wine-installer/opt/wine* ~/wine
sudo -E rm wine*.deb
sudo -E rm -rf wine-installer

sudo -E ln -s ~/wine/bin/wine /usr/local/bin/wine
sudo -E ln -s ~/wine/bin/wineboot /usr/local/bin/wineboot
sudo -E ln -s ~/wine/bin/winecfg /usr/local/bin/winecfg
sudo -E ln -s ~/wine/bin/wineserver /usr/local/bin/wineserver
sudo -E chmod +x /usr/local/bin/wine /usr/local/bin/wineboot /usr/local/bin/winecfg /usr/local/bin/wineserver

sudo -E dpkg --add-architecture armhf && sudo apt update
sudo -E apt install -y libasound2:armhf libc6:armhf libglib2.0-0:armhf libgphoto2-6:armhf libgphoto2-port12:armhf \
    libgstreamer-plugins-base1.0-0:armhf libgstreamer1.0-0:armhf libopenal1:armhf libpcap0.8:armhf \
    libpulse0:armhf libsane1:armhf libudev1:armhf libusb-1.0-0:armhf libvkd3d1:armhf libx11-6:armhf libxext6:armhf \
    libasound2-plugins:armhf ocl-icd-libopencl1:armhf libncurses6:armhf libncurses5:armhf libcap2-bin:armhf libcups2:armhf \
    libdbus-1-3:armhf libfontconfig1:armhf libfreetype6:armhf libglu1-mesa:armhf libglu1:armhf libgnutls30:armhf \
    libgssapi-krb5-2:armhf libkrb5-3:armhf libodbc1:armhf libosmesa6:armhf libsdl2-2.0-0:armhf libv4l-0:armhf \
    libxcomposite1:armhf libxcursor1:armhf libxfixes3:armhf libxi6:armhf libxinerama1:armhf libxrandr2:armhf \
    libxrender1:armhf libxxf86vm1 libc6:armhf libcap2-bin:armhf

# If you keep the WINEDEBUG var, you better get ready for a colossal amount of Wine puke - but at least, long as it does, it means it's running!
#WINEDEBUG=+all wine wineboot
wine wineboot

# winetricks
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod +x winetricks
./winetricks d3dx9 xact

# Now entering the fun part - BLRE
curl -LO https://github.com/SteamRE/DepotDownloader/releases/download/DepotDownloader_2.5.0/DepotDownloader-linux-arm64.zip
unzip DepotDownloader-linux-arm64.zip
rm DepotDownloader-linux-arm64.zip
chmod +x DepotDownloader

STEAM_USERNAME=enter me
curl -LO https://gitlab.com/-/snippets/2529720/raw/main/filelist.txt
./DepotDownloader -app 209870 -depot 209871 -manifest 8740515310020658800 -username $STEAM_USERNAME -filelist filelist.txt

# BLRevive 1.0.3
mkdir -p depots/209871/2520205/Binaries/Win32/Modules
curl -o depots/209871/2520205/Binaries/Win32/BLRevive.dll -L https://gitlab.com/blrevive/blrevive/uploads/f856c0796331bd2777f74cc5e478203f/BLRevive.dll
curl -o depots/209871/2520205/Binaries/Win32/DINPUT8.dll -L https://gitlab.com/blrevive/blrevive/uploads/17d2cca7c37a7da437121bb3bc47d52d/DINPUT8.dll

curl -o depots/209871/2520205/Binaries/Win32/Modules/server-utils.dll -L https://gitlab.com/blrevive/modules/server-utils/uploads/ab28a6242c2d2321f43f7412a6d910f6/server-utils.dll

Xvfb :9874 -screen 0 1024x768x16 &

mkdir -p depots/209871/2520205/FoxGame/Config/BLRevive/server_utils/configs
cat > depots/209871/2520205/FoxGame/Config/BLRevive/aarch64.json << EOF
{
    "Console": {
        "CmdBlacklist": [],
        "CmdWhitelist": [],
        "Enable": false
    },
    "Logger": {
        "FilePath": "blrevive-{server}{timestamp}.log",
        "Level": "info",
        "Target": "file"
    },
    "Modules": {
        "server-utils": {
          "PatchFile": "patches.json",
          "RulesFile": "rules.json"
        }
    },
    "Server": {
        "AuthenticateUsers": false,
        "Enable": true
    }
}
EOF
cat > depots/209871/2520205/FoxGame/Config/BLRevive/server_utils/configs/default.json << EOF
{
  "PatchFile": "patches.json",
  "RulesFile": "rules.json",
  "info": {
      "FileName": "server_info",
      "Host": "0.0.0.0",
      "Port": 7777,
      "Serve": true,
      "ShowSpectatorTeam": false,
      "URI": "/server_info"
  },
  "mutators": {
      "DisableDepots": false,
      "DisableElementalAmmo": false,
      "DisableGear": false,
      "DisableHRV": false,
      "DisableHeadShots": false,
      "DisableHealthRegen": false,
      "DisablePrimaries": false,
      "DisableSecondaries": false,
      "DisableTacticalGear": false,
      "HeadshotsOnly": false,
      "StockLoadout": false
  },
  "properties": {
      "GameForceRespawnTime": 30.0,
      "GameRespawnTime": 3.0,
      "GameSpectatorSwitchDelayTime": 120.0,
      "GoalScore": 4500,
      "MaxIdleTime": 180.0,
      "MinRequiredPlayersToStart": 1,
      "NumEnemyVotesRequiredForKick": 4,
      "NumFriendlyVotesRequiredForKick": 2,
      "PlayerSearchTime": 20.0,
      "RandomBotNames": [
          "Frag Magnet",
          "Spook",
          "OOMKilled",
          "Rakbhu",
          "Server Fault",
          "kernel panic",
          "WINE_CXX_EXCEPTION",
          "ACCESS_VIOLATION",
          "CODE c0000005",
          "Firestarter",
          "Stainless Kill",
          "Bomberman",
          "Wireshark",
          "Sugarfree bot",
          "A real player",
          "James Bot",
          "Quilkin",
          "Clapbot",
          "Prometheus"
      ],
      "TimeLimit": 20,
      "VoteKickBanSeconds": 1200,
      "NumBots": 10,
      "MaxBotCount": 0,
      "MaxPlayers": 32
  }
}
EOF

WINEDLLOVERRIDES="explorer.exe=d;services.exe=d;plugplay.exe=d;svchost.exe=d;rpcss.exe=d;dinput8=n" DISPLAY=:9874 wine depots/209871/2520205/Binaries/Win32/FoxGame-win32-Shipping.exe server HeloDeck?Config=aarch64
```

... And now it actually works, presumably. Stress test incoming.
