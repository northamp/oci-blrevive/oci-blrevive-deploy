# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/tls" {
  version = "4.0.5"
  hashes = [
    "h1:jb/Rg9inGYp4t8HtBoETESsQJgdmOHoe1bzzg2uNB3w=",
    "zh:01cfb11cb74654c003f6d4e32bbef8f5969ee2856394a96d127da4949c65153e",
    "zh:0472ea1574026aa1e8ca82bb6df2c40cd0478e9336b7a8a64e652119a2fa4f32",
    "zh:1a8ddba2b1550c5d02003ea5d6cdda2eef6870ece86c5619f33edd699c9dc14b",
    "zh:1e3bb505c000adb12cdf60af5b08f0ed68bc3955b0d4d4a126db5ca4d429eb4a",
    "zh:6636401b2463c25e03e68a6b786acf91a311c78444b1dc4f97c539f9f78de22a",
    "zh:76858f9d8b460e7b2a338c477671d07286b0d287fd2d2e3214030ae8f61dd56e",
    "zh:a13b69fb43cb8746793b3069c4d897bb18f454290b496f19d03c3387d1c9a2dc",
    "zh:a90ca81bb9bb509063b736842250ecff0f886a91baae8de65c8430168001dad9",
    "zh:c4de401395936e41234f1956ebadbd2ed9f414e6908f27d578614aaa529870d4",
    "zh:c657e121af8fde19964482997f0de2d5173217274f6997e16389e7707ed8ece8",
    "zh:d68b07a67fbd604c38ec9733069fbf23441436fecf554de6c75c032f82e1ef19",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/oracle/oci" {
  version     = "5.25.0"
  constraints = "5.25.0"
  hashes = [
    "h1:8mBjwUUWY2qRHTY3IYEXnTyJvPQPNAhvYrw0xkYOUZA=",
    "zh:11bfa7b4dabbd64aff8203995cdbdbb816c2c980b3cbe5cc9f55fabbc34ea70a",
    "zh:20f2124c52566f92324cfacfd96a14f3af0b3b0f8784a1930e6ddc3d24f697bb",
    "zh:383c4a18cfc6a66cf01bbed5295e3b7913b9551e732fb06faaa8ff5dcdf279ea",
    "zh:5d3689ffdb06984326d76acb69d509f51fc3b54ff4347a49a1f6f75a7404ec8b",
    "zh:68a28901e50c7be2ceddd3e4432b0ce78055077dd99ebbc39ea0139a06cea627",
    "zh:6e3093b2eec0fd495d0bc9c4bd172ffdbe864d257dea28f1eda2069dcfca1823",
    "zh:810229cb987c7752c3513b6d39b69f76fa120dde4759ac2ec5b4cd2266243588",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a44c6a771baae6c0aa0d476a2f1364b4cbeb824e72d6b717d62cf9ce01b33a8f",
    "zh:b557b268fb3f96a3da0a3d1b2ccea547a38034b546bba283a5f1bb8a2738e74c",
    "zh:dfaccdc3ea84725e6e98e11a48731fb535cfa5d03cbcedb2f478b8c2ed42fea3",
    "zh:e7a0c1ae0ae0fb8fbe78602a5e6e05ff0b25f179e2966b41801925f08b85856a",
    "zh:ef4b8c7cc88ff0be8c817e11c69fd01911d9e30ebdb6d75f2123e25b1547946d",
    "zh:f00515df4dad7c125d49dd3eb03701c21ded437774c5ce0339f5868277fb848d",
    "zh:fc1abb736fd67eb16003eae5341ac95746aa468a966d9990ed22bccd81944635",
  ]
}
